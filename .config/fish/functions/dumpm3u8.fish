function dumpm3u8
    set -f _m3u8 $argv[1]
    set -f _destiny $argv[2]
    ffmpeg -protocol_whitelist file,http,https,tcp,tls,crypto -i "$_m3u8" -c copy "$_destiny"
end
