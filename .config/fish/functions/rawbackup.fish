function rawbackup
    set -f _name (basename $argv[1])
    dd conv=fsync status=progress bs=4K if=$argv[1] | zstd -T0 -18 - -o rawbackup_$_name.bin.zst
end
