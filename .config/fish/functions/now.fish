function now
    set -f _RESULT (date --date="$argv")
    if test $status -eq 0
        echo
        echo "Result of your query:"
        echo $_RESULT
        echo
    else
        echo
        echo "Examples:"
        echo
        echo "now 167 minutes"
        echo "now 2022-01-10 20:20 3 days "
        echo "now 2023-12-12 20:20 90 minutes ago"
        echo "Other keywords: days, seconds, ..."
        echo
        echo "Note: when indicating time don't use +, for example, 20:35 +2 minutes, because that's interpreted as"
        echo "a timezone offset"
        echo
    end
end
