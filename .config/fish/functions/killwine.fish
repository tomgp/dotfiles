function killwine
    set -f _pid (pgrep -fi '\.exe')
    if test $status -eq 0
        echo "$_pid" | cut -d ' ' -f1 | xargs kill -9
    end

    set _pid (pgrep -fi 'wine')
    if test $status -eq 0
        echo "$_pid" | cut -d ' ' -f1 | xargs kill -9
    end
end
