function pacexplicit
    expac "%n %N" -Q (expac "%n %G" | grep -Ev (string join '|' (begin; expac -l '\n' '%E' base; expac -l '\n' '%E' base-devel; end)) | awk '$2 == "" {print $1}')
end
