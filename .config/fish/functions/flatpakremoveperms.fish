function flatpakremoveperms
    flatpak permissions | grep -i '^documents' | column -t -o ';' | cut -d ';' -f 2 | xargs -n1 flatpak permission-remove -vv documents
end
