function usersharew
    set -f _share $argv[1]
    set -f _dir $argv[2]
    net usershare add $_share $_dir '' 'everyone:f' 'guest_ok=yes'
end
