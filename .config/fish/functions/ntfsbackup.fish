function ntfsbackup
    set -f _name (basename $argv[1])
    ntfsclone --save-image -o - $argv[1] | zstd -T0 -18 - -o ntfsbackup_$_name.img.zst
end
