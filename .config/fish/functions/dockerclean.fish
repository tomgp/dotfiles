function dockerclean
    docker rm -a -f
    docker rmi -a -f
    docker container rm -a -f
    docker image rm -a -f
    yes | docker system prune -a --volumes
    yes | podman system reset
end
