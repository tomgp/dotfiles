function prefixrename
    set -f index 1
    set -f prefix $argv[1]

    if test -e $prefix
        echo 'Usage: prefix [files]'
        return 1
    end

    for file in $argv[2..]
        set -f per_parts (basename $file | string split .)
        set -f extension (string join '.' $per_parts[2..])
        set -f padded_index (string pad -c '0' -w 3 $index)
        set -f destination (dirname $file)/$prefix$padded_index.$extension
        echo mv $file $destination
        set -f index (math $index + 1)
        mv $file $destination
    end
end
