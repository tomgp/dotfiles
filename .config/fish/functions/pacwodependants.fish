function pacwodependants
    expac -H M "%011m\t%-20n\t%10d" (comm -23 (pacman -Qqen | sort | psub) (begin;
    expac -l '\n' '%E' base-devel
    expac -l '\n' '%E' base
  end | sort | uniq | psub)) | sort -n
end
