function cleanwine
    rm -vf ~/.local/share/applications/wine-extension*
    rm -vf ~/.local/share/icons/hicolor/*/*/application-x-wine-extension*
    rm -vf ~/.local/share/mime/packages/x-wine*
    rm -vf ~/.local/share/mime/application/x-wine-extension*
    rm -vf ~/.local/share/applications/mimeinfo.cache
    update-desktop-database ~/.local/share/applications
    update-mime-database ~/.local/share/mime/
end
