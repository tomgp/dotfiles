function pactmpfiles
    for i in /usr/lib/tmpfiles.d/*.conf
        pacman -Qo $i
    end | cut -d ' ' -f5 | sort -u
end
