function usersharer
    set -f _share $argv[1]
    set -f _dir $argv[2]
    net usershare add $_share $_dir '' '' 'guest_ok=yes'
end
