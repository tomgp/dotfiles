function udevinfo
    set -f _UDEVPATH $argv[1]

    set -f _TEMP (echo -n "$_UDEVPATH" | grep '^/dev/')
    if not test -z $_TEMP
        set _UDEVPATH (udevadm info --query=path --name=$_UDEVPATH)
    end

    udevadm info --attribute-walk --path=$_UDEVPATH
end
