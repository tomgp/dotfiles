function sslcert
    set -f _host $argv[1]
    set -f _port $argv[2]
    test -z $_port; and set _port 443

    begin
        set -l IFS
        set -f _sslcert (echo | openssl s_client -connect $_host:$_port 2>&1)
    end

    echo "$_sslcert"
    echo
    echo "$_sslcert" | openssl x509 -text
    echo "$_sslcert" | openssl x509 -fingerprint -noout -md5
    echo "$_sslcert" | openssl x509 -fingerprint -noout -sha1
    echo "$_sslcert" | openssl x509 -fingerprint -noout -sha256
end
