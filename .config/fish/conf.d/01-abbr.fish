# Command substitutions
abbr ls 'lsd --color auto -F --group-dirs first'
abbr ll 'ls -l'
abbr l 'ls -l'
abbr grep 'grep --color=auto -Ei'
abbr pgrep 'pgrep -fa'
abbr watch ' watch -cd'
abbr vi nvim
abbr vim nvim
abbr sudo 'command sudo '
abbr mc 'mc -u'
abbr makepkg 'makepkg -f -s --asdeps --needed'
abbr cryptsetup 'cryptsetup -v'
abbr clamscan 'clamscan -v -a -o -r'
abbr less $PAGER
abbr more $PAGER
abbr whois 'whois -H'
abbr dd 'dd status=progress'
abbr rg 'rg -S'
abbr ip 'ip -c'
abbr cat bat
abbr dmesg 'journalctl -ke --no-hostname'
abbr pkill 'pkill -ei'
abbr jpegoptim 'jpegoptim -f -w 4 -s -v --all-progressive'

abbr gzip pigz
abbr gunzip unpigz
abbr unzip2 pbunzip2
abbr bzip2 bunzip2
abbr bzcat pbzcat

# Other abrreviations
abbr photocopy 'scanimage --mode lineart --resolution 300 --format tiff -p -x 215 -y 297 --batch="scan%d.tiff" --batch-prompt'
abbr photocopycolor 'scanimage --mode color --resolution 300 --format tiff -p -x 215 -y 297 --batch="scan%d.tiff" --batch-prompt'
abbr duf 'du -xhd1 --exclude=nobackup | sort -h'
abbr dufa 'du -xha --exclude=nobackup | sort -h'
abbr gccdefaultmacros 'echo | gcc -E -dM -'
abbr etcmodified 'find /etc -type f -exec pacman -Qqo {} 2> /dev/null \; | sort -u | xargs pacman -Qii | grep "^MODIFIED"'
abbr etcnotfound 'find /etc -type f -exec pacman -Qqo {} > /dev/null \;'
abbr pacbor 'sudo pacman -Rscn'
abbr pacs 'paru -Ss'
abbr pacorphans 'sudo pacman -Rns $(pacman -Qtdq)'
abbr cpio_extract 'cpio -id --no-absolute-filenames'
abbr traceapp 'strace -D -tt -T -x -y -f -s 10000 -v -o traceapp_log.txt'
abbr rename 'qmv -f destination-only'
abbr makepatch 'diff -ruN'
abbr secrm ' shred -f -n 1 -u -v --random-source=/dev/urandom'
#abbr androidrun 'am start -a android.intent.action.MAIN -n '
abbr nocomments 'grep --color=auto -vE \'^$|^#|^;\''
abbr mksrcinfo 'makepkg --printsrcinfo > .SRCINFO'
abbr ffify 'yes \xff | tr -d "\n" | dd status=progress'
abbr 00ify 'dd status=progress if=/dev/zero'
abbr procwatch "while (true); do date; top -Hbn1 | sed -n '7,12'p; sleep 2; done"
abbr rdocs 'xdg-open $(rustup doc --path)'
abbr generateserverpki 'openssl req -nodes -newkey rsa:4096 -keyout myserver.key -out server.csr'
abbr pipewire-restart 'systemctl --user daemon-reload && systemctl --user restart pipewire.socket pipewire pipewire-pulse.socket pipewire-pulse.service wireplumber.service'
abbr rsynccopy 'rsync --open-noatime --stats -rxvvtP --delete --delete-excluded'
abbr flatpaklist 'flatpak --columns=app,name,size,installation --runtime list'
abbr usershare 'net usershare info'
abbr pipewire-debug "systemctl --user set-environment WIREPLUMBER_DEBUG=D"
abbr randhex "openssl rand -hex"
abbr fontfilelist "fc-list ':' file | sort | cut -d ':' -f 1"
abbr fontownedlist "fc-list ':' file | cut -d ':' -f 1 | xargs pacman -Qo | cut -d ' ' -f 5 | sort -u"
abbr jflags 'java -XX:+UnlockDiagnosticVMOptions -XX:+UnlockExperimentalVMOptions -XX:+PrintFlagsFinal -version'
abbr jsettings 'java -XshowSettings -version'
abbr jcliflags 'java -XX:+PrintCommandLineFlags -XX:+PrintGCDetails -version'
abbr orphanetcconfs "find /etc -type f -exec pacman -Qo {} \; 2>&1 | grep -vE 'is owned|.git|archive|backup|cadir' | cut -d ' ' -f 5"
abbr pacaltered 'pacman -Qkk 2> pacaltered_errors.log | grep -v \'0 altered files$\' > pacaltered.log'
abbr abs 'pkgctl repo clone'
abbr uvsync 'uv pip install --exact --strict -r'
abbr systemdlinks 'find /etc/systemd -type l -exec test -f {} \; -print | awk -F\'/\' \'{ printf ("%-40s | %s\n", $(NF-0), $(NF-1)) }\' | sort -f'
abbr rembg 'uv tool run --python cpython-3.9.21-linux-x86_64-gnu rembg[cpu,cli]'
