function envsource
    for line in (/usr/bin/cat $argv | /usr/bin/grep -vE '^#|^$')
        set -l item (string split -m 1 '=' $line)
        set -l key $item[1]
        set -l value $item[2]

        if test $key = PATH
            set -l paths (echo $value | string split ':')
            for apath in $paths
                if test $apath = '$PATH'
                    continue
                end

                fish_add_path -g -a -P (echo $apath | envsubst)
            end
        else
            #echo "Exporting key $key with value $value..."
            set -gx $key (echo $value | envsubst)
        end
    end
end

for conffile in $HOME/.config/environment.d/*.conf
    #    echo "Loading $conffile"
    envsource $conffile
end
